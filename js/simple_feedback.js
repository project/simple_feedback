/**
 * @File
 * Javascript for the Simple Feedback module.
 */

(function ($) {
  'use strict';

  Drupal.behaviors.simpleFeedback = {
    attach: function (context, settings) {
      $.get('/ajax/simple_feedback/get_values/' + Drupal.settings.simpleFeedback.nid, function (data) {
        $('#yes-vote').html('(' + data.count.yes + ')');
        $('#no-vote').html('(' + data.count.no + ')');
      });
    }
  };
}(jQuery));
